<h1 align="center">Hello 👋, I'm Ghassan</h1>
<h3 align="center">A M.Sc. student in Bioinformatics with a specialization in software development and data analysis.</h3>

- 🌱 I’m currently learning **_computational neuroscience_, _machine learning_, and _bioinformatics_ (and pretty much everything else)**

- 📫 How to reach me **dabane.ghassan@gmail.com**

<h3> Languages and tools i use : </h3>
<p align="left">
  <img src="https://devicons.github.io/devicon/devicon.git/icons/python/python-original.svg" alt="python" width="40" height="40"/>
  <img src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/r/r.png" alt="R" width="40" height="40"/>
  <img src="https://github.com/devicons/devicon/blob/master/icons/ubuntu/ubuntu-plain-wordmark.svg" alt="ubuntu" width="40" height="40"/>  
  <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/>
  <img src="https://devicons.github.io/devicon/devicon.git/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/>
  <img src="https://devicons.github.io/devicon/devicon.git/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/>
  <img src="https://devicons.github.io/devicon/devicon.git/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40"/>
  <img src="https://devicons.github.io/devicon/devicon.git/icons/php/php-original.svg" alt="php" width="40" height="40"/>
  <img src="https://github.com/devicons/devicon/blob/master/icons/visualstudio/visualstudio-plain.svg" alt="VS" width="40" height="40"/>
  <img src="https://github.com/spyder-ide/spyder/blob/master/spyder/images/spyder.svg" alt="spyder" width="40" height="40"/>
  <img src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/jupyter-notebook/jupyter-notebook.png" alt="jupyter" width="40" height="40"/> 
  <img src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/latex/latex.png" alt="latex" width="40" height="40"/> 
  
</p>

<h3> My Github Stats: </h3>
<p>&nbsp;<img align="center" src="https://github-readme-stats.vercel.app/api?username=dabane-ghassan&show_icons=true&theme=chartreuse-dark&hide=issues&count_private=true" alt="dabane-ghassan" />
</p>


<h4 align="center"> Connect with me </h4>
<p align="center">
<a href="https://fb.com/ghassan.dabane.97" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/facebook.svg" alt="https://www.facebook.com/ghassan.dabane.97" height="30" width="30" /></a>
<a href="https://instagram.com/dabane_ghassan" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/instagram.svg" alt="https://www.instagram.com/dabane_ghassan" height="30" width="30" /></a>
</p>

